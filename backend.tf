terraform {
  backend "s3" {
    bucket         = aws_s3_bucket.terraform_state
    key            = "global/s3/terraform.tfstate"
  }
}
